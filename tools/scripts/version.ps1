. "./tools/scripts/settings.ps1"

$localVersion = Get-Content -Raw -Path ./version.json | ConvertFrom-Json

$majorVersion = $localVersion.major
$minorVersion = $localVersion.minor
$patch = $localVersion.patch

$branch = @{ $true = $Env:BUILD_SOURCEBRANCHNAME; $false = $(git symbolic-ref --short -q HEAD) }[$Env:BUILD_SOURCEBRANCHNAME -ne $NULL];
$sanitisedBranch = "$($branch.Replace("/", "."))"

# If building locally, there's no build number, so use 'local';
$buildRevision = @{ $true = $Env:BUILD_BUILDNUMBER; $false = "local" }[$Env:BUILD_BUILDNUMBER -ne $NULL];

$sourceRevisionId = $(git rev-parse --short HEAD)

# If SourceLink is enabled, it automatically suffixes the informational version with the Git revision
# If not we need to add it
#$buildMetaData = "$buildRevision.$sourceRevisionId"
$buildMetaData = "$buildRevision"

$tag = @{ $true = ""; $false = "-$sanitisedBranch+$buildMetaData.$sourceRevisionId" }[$branch -eq $buildSettings.mainBranchName];

# Diagnostics
Write-Output "   branch:              $($branch)"
Write-Output "   sanitisedBranch:     $($sanitisedBranch)"
Write-Output "   buildRevision:       $($buildRevision)"
Write-Output "   sourceRevisionId:    $($sourceRevisionId)"
Write-Output "   buildMetaData:       $($buildMetaData)"
Write-Output "   tag:                 $($tag)"
Write-Output ""

$baseVersion = "$majorVersion.$minorVersion.$patch"

$version = "$baseVersion$tag"
$assemblyVersion = "$majorVersion.0.0.0"
$informationalVersion = @{ $true = "$baseVersion+$buildMetaData"; $false = "$baseVersion-$sanitisedBranch+$buildMetaData" }[$branch -eq $buildSettings.mainBranchName];
$fileVersion = "$baseVersion"
$dockerVersion = $version -replace '\+', '-'

Write-Output "   version:              $($version)"
Write-Output "   assemblyVersion:      $($assemblyVersion)"
Write-Output "   informationalVersion: $($informationalVersion).$($sourceRevisionId)"
Write-Output "   fileVersion:          $($fileVersion)"
Write-Output "   dockerVersion         $($dockerVersion)"
Write-Output ""

# Create Version variables for DevOps build tasks
Write-Host "   ##vso[task.setvariable variable=Version]$version"
Write-Host "   ##vso[task.setvariable variable=AssemblyVersion]$assemblyVersion"
Write-Host "   ##vso[task.setvariable variable=InformationalVersion]$informationalVersion"
Write-Host "   ##vso[task.setvariable variable=FileVersion]$fileVersion"
Write-Host "   ##vso[task.setvariable variable=DockerVersion]$dockerVersion"
Write-Host ""