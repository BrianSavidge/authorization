$buildSettings = Get-Content -Raw -Path ./build-settings.json | ConvertFrom-Json

$name = $buildSettings.name
$productGuid = $buildSettings.productGuid
$nameKebabCase = $buildSettings.name.ToLower().Replace(".","-")
$nameHireachical = $buildSettings.name.ToLower().Replace(".","/")
$configuration = "Release"
$outputDirectory = @{ $true = $Env:BUILD_ARTIFACTSTAGINGDIRECTORY; $false = "./build-artifacts" }[$Env:BUILD_ARTIFACTSTAGINGDIRECTORY -ne $NULL];

Write-Output "   name:                 $($name)"
Write-Output "   nameKebabCase:        $($nameKebabCase)"
Write-Output "   nameHireachical:      $($nameHireachical)"
Write-Output "   productGUID:          $($productGuid)"
Write-Output "   configuration:        $($configuration)"
Write-Output "   outputDirectory:      $($outputDirectory)"
Write-Output ""

Write-Host "##vso[task.setvariable variable=projectNameKebabCase]$nameKebabCase"
Write-Host "##vso[task.setvariable variable=projectNameHireachical]$nameHireachical"
Write-Host "##vso[task.setvariable variable=productGuid]$productGuid"