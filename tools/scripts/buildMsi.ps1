Write-Output ""
. "./tools/scripts/version.ps1"
. "./tools/scripts/settings.ps1"

.\tools\nuget.exe install `
    Actisure.Utilities.BuildMsi `
    -configfile ./nuget.Config `
    -OutputDirectory "$outputdirectory/msi/packages"

$absoluteOutputDirectory = Resolve-Path($outputDirectory)

.\tools\vsmsbuild `
    $outputdirectory\msi\packages\Actisure.Utilities.BuildMsi.1.9.4\tools\src\build-msi.proj `
    /p:configuration=Release `
    /p:Version=$fileVersion `
    /p:APPLICATIONNAME=$name `
    /p:ShortAppName=$name `
    /p:AppName=$name `
    /p:VirtualDirectoryName=$name `
    /p:ProductGUID=$productGUID `
    /p:BasePath="$absoluteOutputDirectory/publish" `
    /p:OutputPath="$absoluteOutputDirectory/msi" `
    /r