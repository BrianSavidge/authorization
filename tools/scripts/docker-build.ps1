$repoRoot = (get-item $PSScriptRoot).Parent.Parent.FullName

docker build $repoRoot -t pacman.actisure.co.uk/docker/actisure/actisure/services/authorization -f $repoRoot\src\Actisure.Services.Authorization\Dockerfile