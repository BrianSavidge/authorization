Write-Output ""
. "./tools/scripts/version.ps1"
. "./tools/scripts/settings.ps1"

$chocoDirectory = "./deployment/chocolatey"
$chocoToolsDirectory = "$chocoDirectory/tools"

Copy-Item "$outputDirectory/msi/*.msi" -Destination $chocoToolsDirectory -Force

choco pack "$chocoDirectory/$nameKebabCase.nuspec" --version $fileVersion --outputdirectory $outputDirectory

if(Test-Path "$chocoToolsDirectory/*.msi") { Remove-Item "$chocoToolsDirectory/*.msi" -Force }