@echo off
@REM cls

GOTO CheckOSType
REM Make sure the Actisure package folder is set if its not already set
IF "%ACTISURE_PACKAGE_FOLDER%" NEQ "" GOTO CheckOSType
rem set the local environment
set ACTISURE_PACKAGE_FOLDER=c:\Actisure_Packages
set NUGET_PACKAGES=%ACTISURE_PACKAGE_FOLDER%\NUGET
rem and make it a permanent for the user
setx ACTISURE_PACKAGE_FOLDER %ACTISURE_PACKAGE_FOLDER%
setx NUGET_PACKAGES %NUGET_PACKAGES%


:CheckOSType
rem work out where things are stored (32bit v 64bit)
if DEFINED ProgramFiles(x86) GOTO x86
rem this is a 32bit machine
SET ProgramFilesDir=%ProgramFiles%
goto runVSWhere

:x86
rem this is a 64bit machine
SET ProgramFilesDir=%ProgramFiles(x86)%
goto runVSWhere


:runVSWhere
rem Generate full path to VSWHERE (part of VS2017U2)
set "VSWHERE=%ProgramFilesDir%\Microsoft Visual Studio\Installer\vswhere.exe"
if not exist "%VSWHERE%" goto VSWhereMissing

rem extract the install directory (different versions of Visual Studio install in different locations)
for /f "usebackq tokens=*" %%i in (`"%VSWHERE%" -latest -products * -requires Microsoft.Component.MSBuild -property installationPath`) do (
  set InstallDir=%%i
)

rem check for Visual Studio 2019 / Current
:CheckForVS2019orCurrent
if not exist "%InstallDir%\MSBuild\Current\Bin\MSBuild.exe" goto CheckForVS2017
rem run the build
echo Running: "%InstallDir%\MSBuild\Current\Bin\MSBuild.exe" %*
"%InstallDir%\MSBuild\Current\Bin\MSBuild.exe" %*
goto end

:CheckForVS2017
if not exist "%InstallDir%\MSBuild\15.0\Bin\MSBuild.exe" goto MissingVisualStudioMSBuild
rem run the build
echo Running: "%InstallDir%\MSBuild\15.0\Bin\MSBuild.exe" %*
"%InstallDir%\MSBuild\15.0\Bin\MSBuild.exe" %*
goto end

:MissingVisualStudioMSBuild
echo Unable to find MSBuild in: "%InstallDir%\MSBuild\15.0\Bin\MSBuild.exe" or "%InstallDir%\MSBuild\Current\Bin\MSBuild.exe"
goto DefaultToDotNET

:VSWhereMissing
echo Unable to find VSWhere.  Expecting the file in: %VSWHERE%
goto DefaultToDotNET

:DefaultToDotNET
echo Defaulting to .NET MSBuild
"%windir%\Microsoft.NET\Framework\v4.0.30319\MSBuild.exe" %*
goto end

:end