﻿using System;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;

using Actisure.Services.Authorization.Infrastructure.Certificates;

using FluentAssertions;

using Xunit;

namespace Actisure.Services.Authorization.Tests.Unit
{
    public class CertificateProviderTests
    {
        [Theory]
        [InlineData("")]
        [InlineData(null)]
        [InlineData("the-wrong-password")]
        public void GetCertificate_EmbeddedCertWithInvalidPassword_ThrowsException(string testPassword)
        {
            Func<X509Certificate2> act =
                () => CertificateProvider.GetCertificate(
                    new CertificateOptions
                    {
                        Source = CertificateSources.Embedded,
                        EmbeddedCertificatePassword = testPassword
                    });

            act.Should().Throw<CryptographicException>("the correct password for the embedded certificate has not been supplied.");
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        [InlineData(" ")]
        public void GetCertificate_StoreBasedCertificateAndMissingStoreName_ThrowsException(string storeName)
        {
            Func<X509Certificate2> act =
                () => CertificateProvider.GetCertificate(
                    new CertificateOptions
                    {
                        Source = CertificateSources.CertificateStore,
                        StoreName = storeName,
                        Thumbprint = "thumbprint"
                    });

            act.Should().Throw<ArgumentException>("trying to open a store with a missing name is going to fail.");
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        [InlineData(" ")]
        public void GetCertificate_StoreBasedCertificateAndMissingThumbprint_ThrowsException(string thumbprint)
        {
            Func<X509Certificate2> act =
                () => CertificateProvider.GetCertificate(
                    new CertificateOptions
                    {
                        Source = CertificateSources.CertificateStore,
                        StoreName = StoreName.My.ToString(),
                        Thumbprint = thumbprint
                    });

            act.Should().Throw<ArgumentException>("trying to find a certificate with a missing thumbprint is going to fail.");
        }

        [Fact]
        public void GetCertificate_NoSpecifiedSource_DefaultsToEmbeddedCertificate()
        {
            var certificate =
                CertificateProvider.GetCertificate(
                    new CertificateOptions
                    {
                        EmbeddedCertificatePassword = "TYyeB3meVHNGrpQx"
                    });

            certificate.Should().NotBeNull("using the embedded certificate is the default option and the matching password has been supplied.");
        }

        [Fact]
        public void GetCertificate_StoreBasedCertificateAndInvalidStoreName_ThrowsException()
        {
            Func<X509Certificate2> act =
                () => CertificateProvider.GetCertificate(
                    new CertificateOptions
                    {
                        Source = CertificateSources.CertificateStore,
                        StoreName = "this-store-probably-doesn't-exist",
                        Thumbprint = "thumbprint"
                    });

            act.Should().Throw<CryptographicException>("trying to open a store with an invalid name is going to fail.");
        }

        [Fact]
        public void GetCertificate_StoreBasedCertificateAndInvalidThumbprint_ThrowsException()
        {
            Func<X509Certificate2> act =
                () => CertificateProvider.GetCertificate(
                    new CertificateOptions
                    {
                        Source = CertificateSources.CertificateStore,
                        StoreName = StoreName.My.ToString(),
                        Thumbprint = "this-thumbprint-probably-doesn't-exist"
                    });

            act.Should().Throw<InvalidOperationException>("trying to find a certificate with an invalid thumbprint is going to fail.");
        }

        [Fact]
        public void GetCertificate_StoreBasedInvalidCertificate_ThrowsException()
        {
            var certificateStore = new X509Store(StoreName.My, StoreLocation.LocalMachine);

            certificateStore.Open(OpenFlags.ReadOnly | OpenFlags.OpenExistingOnly);

            var certificates =
                certificateStore.Certificates.Find(X509FindType.FindByTimeValid, DateTime.Now, false);

            if (certificates.Count > 0)
            {
                foreach (var certificate in certificates)
                {
                    if (!certificate.Verify())
                    {
                        Func<X509Certificate2> act =
                            () => CertificateProvider.GetCertificate(
                                new CertificateOptions
                                {
                                    Source = CertificateSources.CertificateStore,
                                    StoreName = StoreName.My.ToString(),
                                    Thumbprint = certificate.Thumbprint
                                });

                        act.Should().Throw<InvalidOperationException>("trying to find an invalid certificate is going to fail.");
                    }
                }
            }
        }

        [Fact]
        public void GetCertificate_StoreBasedCertificateWithoutPrivateKey_ThrowsException()
        {
            var certificateStore = new X509Store(StoreName.My, StoreLocation.LocalMachine);

            certificateStore.Open(OpenFlags.ReadOnly | OpenFlags.OpenExistingOnly);

            var certificates =
                certificateStore.Certificates.Find(X509FindType.FindByTimeValid, DateTime.Now, false);

            if (certificates.Count > 0)
            {
                foreach (var certificate in certificates)
                {
                    if (certificate.Verify() && !certificate.HasPrivateKey)
                    {
                        Func<X509Certificate2> act =
                            () => CertificateProvider.GetCertificate(
                                new CertificateOptions
                                {
                                    Source = CertificateSources.CertificateStore,
                                    StoreName = StoreName.My.ToString(),
                                    Thumbprint = certificate.Thumbprint
                                });

                        act.Should().Throw<InvalidOperationException>("trying to find a certificate without a private key is going to fail.");
                    }
                }
            }
        }

        [Fact]
        public void GetCertificate_StoreBasedValidCertificateWithPrivateKey_ReturnsCertificate()
        {
            var certificateStore = new X509Store(StoreName.My, StoreLocation.LocalMachine);

            certificateStore.Open(OpenFlags.ReadOnly | OpenFlags.OpenExistingOnly);

            var certificates =
                certificateStore.Certificates.Find(X509FindType.FindByTimeValid, DateTime.Now, false);

            if (certificates.Count > 0)
            {
                foreach (var certificate in certificates)
                {
                    if (certificate.Verify() && certificate.HasPrivateKey)
                    {
                        var result = 
                            CertificateProvider.GetCertificate(
                                new CertificateOptions
                                {
                                    Source = CertificateSources.CertificateStore,
                                    StoreName = StoreName.My.ToString(),
                                    Thumbprint = certificate.Thumbprint
                                });

                        result.Should().BeOfType<X509Certificate2>();
                        result.Should().NotBeNull();
                    }
                }
            }
        }
    }
}
