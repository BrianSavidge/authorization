﻿using Actisure.Services.Authorization.Infrastructure;

using FluentAssertions;

using Xunit;

namespace Actisure.Services.Authorization.Tests.Unit
{
    public class AcrValuesParserTests
    {
        [Theory]
        [InlineData("", "")]
        [InlineData("   ", "")]
        [InlineData("", "   ")]
        public void TryParse_InvalidValueAndInvalidKey_ReturnsFalse(
            string value,
            string key)
        {
            AcrValuesParser
                .TryParse(value, key, out var actAs)
                .Should().BeFalse($"value is '{value}' and key is '{key}'");

            actAs.Should().BeNull("value couldn't be parsed");
        }

        [Theory]
        [InlineData("no-colon-delimiter")]
        [InlineData("delimiter-but-no-value:")]
        [InlineData(":delimiter-but-no-key")]
        //[InlineData("too-many::delimiters")]
        public void TryParse_MalformedValue_ReturnsFalse(string value)
        {
            AcrValuesParser
                .TryParse(value, "delimiter", out var actAs)
                .Should().BeFalse("value is malformed");

            actAs.Should().BeNull("value couldn't be parsed");
        }

        [Fact]
        //[InlineData("too-many::delimiters")]
        public void TryParse_MutipleValuesContainingMatchingKey_ReturnsTrue()
        {
            // ACR values are space delimited
            const string acrValues = "act_as:test.user idp:name_of_idp tenant:name_of_tenant";

            AcrValuesParser
                .TryParse(acrValues, "act_as", out var actAs)
                .Should().BeTrue("value is valid and contains key");

            actAs.Should().Be("test.user", "test.user is the value we were looking for");
        }

        [Fact]
        //[InlineData("too-many::delimiters")]
        public void TryParse_MutipleValuesWithNoMatchingKey_ReturnsFalse()
        {
            // ACR values are space delimited
            const string spaceDelimitedAcrValues = "act_as:test.user idp:name_of_idp tenant:name_of_tenant";

            AcrValuesParser
                .TryParse(spaceDelimitedAcrValues, "you_wont_find_me", out var soughtValue)
                .Should().BeFalse("there's no value with that key");

            soughtValue.Should().BeNull();
        }

        [Fact]
        //[InlineData("too-many::delimiters")]
        public void TryParse_MutipleValuesWithTheSameKey_ReturnsTrueWithFirstValue()
        {
            // ACR values are space delimited
            const string acrValues = "act_as:first.user act_as:second.user act_as:third.user";

            AcrValuesParser
                .TryParse(acrValues, "act_as", out var actAs)
                .Should().BeTrue("value is valid and contains key");

            actAs.Should().Be("first.user", "first.user is the value we were looking for");
        }

        [Fact]
        public void TryParse_NullValueAndNullKey_ReturnsFalse()
        {
            AcrValuesParser
                .TryParse(null, null, out _)
                .Should().BeFalse("value is null and key is null");
        }

        [Fact]
        public void TryParse_NullValueWithKey_ReturnsFalse()
        {
            AcrValuesParser
                .TryParse(null, "act_as", out _)
                .Should().BeFalse("value is null");
        }

        [Fact]
        //[InlineData("too-many::delimiters")]
        public void TryParse_ValidValue_ReturnsTrue()
        {
            AcrValuesParser
                .TryParse("act_as:test.user", "act_as", out var actAs)
                .Should().BeTrue("value is valid and contains key");

            actAs.Should().Be("test.user", "test.user is the value we were looking for");
        }

        [Fact]
        public void TryParse_ValueWithNullKey_ReturnsFalse()
        {
            AcrValuesParser
                .TryParse("acr_values=act_as:test.user", null, out _)
                .Should().BeFalse("key is null");
        }
    }
}
