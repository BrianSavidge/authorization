﻿using System.Collections.Generic;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;

using Serilog;
using Serilog.Events;

namespace Actisure.Services.Authorization.Tests
{
    public static class HostFactory
    {
        public static async Task<IHost> Create<TStartup>(IEnumerable<KeyValuePair<string, string>> configSettings = null) where TStartup : class
        {
            Log.Logger =
                new LoggerConfiguration()
                    .MinimumLevel.Debug()
                    .MinimumLevel.Override("Microsoft", LogEventLevel.Warning)
                    .MinimumLevel.Override("Microsoft.Hosting.Lifetime", LogEventLevel.Information)
                    .MinimumLevel.Override("System", LogEventLevel.Warning)
                    .MinimumLevel.Override("Microsoft.AspNetCore.Authentication", LogEventLevel.Information)
                    .Enrich.FromLogContext()
                    .WriteTo.Debug(outputTemplate: "[{Timestamp:HH:mm:ss} {Level:u3}] {Message:lj} {Properties:j}{NewLine}{Exception}")
                    .CreateLogger();

            var hostBuilder =
                    new HostBuilder()
                        .ConfigureWebHost(webHost =>
                        {
                            webHost
                                .UseTestServer()
                                .UseStartup<TStartup>()
                                .UseSerilog();
                        })
                ;

            hostBuilder.ConfigureAppConfiguration((hostingContext, config) =>
            {
                config.AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
                config.AddInMemoryCollection(configSettings);
            });

            var host = await hostBuilder.StartAsync();

            // Need this for faking Flurl requests/responses 
            host.GetTestServer().PreserveExecutionContext = true;

            return host;
        }
    }
}
