﻿using System.Net;

using Flurl.Http.Testing;

namespace Actisure.Services.Authorization.Tests
{
    internal static class HttpTestSetupExtensions
    {
        public static void SetFakeAuthenticationServiceToRespondWith(this HttpTestSetup httpTest, HttpStatusCode httpStatusCode)
        {
            httpTest.RespondWith(string.Empty, (int) httpStatusCode);
        }

        public static HttpTest RespondWithValidLogin(this HttpTest fakeService)
        {
            fakeService
                .ForCallsTo("*api/authentication/login")
                .RespondWith();

            return fakeService;
        }

        public static HttpTest RespondWithNoSecurityFunctions(this HttpTest fakeService)
        {
            fakeService
                .ForCallsTo("*api/authentication/getfunctionsforuser*")
                .RespondWithJson(
                    new
                    {
                        success = true,
                        SecAccessTeamList = new object[0],
                        SecAccessUserList = new object[0]
                    });

            return fakeService;
        }
    }
}
