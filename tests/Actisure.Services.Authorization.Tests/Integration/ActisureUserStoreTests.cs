﻿using System;
using System.Net;
using System.Threading.Tasks;

using Actisure.Services.Authorization.Infrastructure;
using Actisure.Services.Authorization.Infrastructure.ProfileService;
using Actisure.Services.Authorization.Infrastructure.Resources;

using AutoFixture;
using AutoFixture.AutoNSubstitute;

using FluentAssertions;

using Flurl.Http;
using Flurl.Http.Testing;

using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Localization;

using NSubstitute;

using Xunit;

namespace Actisure.Services.Authorization.Tests.Integration
{
    public class ActisureUserStoreTests
    {
        private const string GetUserIsValidRoute = "*api/authentication/getuserisvalid*";
        private const string GetFunctionsForUserRoute = "*api/authentication/getfunctionsforuser*";

        public ActisureUserStoreTests()
        {
            _fixture.Register(() =>
            {
                var fakeService = new HttpTest();
                fakeService.RespondWithValidLogin();

                return fakeService;
            });

            _fixture.Register(() =>
            {
                var fakeConfiguration = Substitute.For<IConfiguration>();
                fakeConfiguration[ConfigurationKeys.AuthenticationServiceBaseAddress].Returns("http://fake-authentication-service");

                return new ActisureUserStore(fakeConfiguration, Substitute.For<IStringLocalizer<Shared>>());
            });
        }

        private readonly IFixture _fixture = new Fixture().Customize(new AutoNSubstituteCustomization());

        [Theory]
        [InlineData(HttpStatusCode.NotFound)]
        [InlineData(HttpStatusCode.Unauthorized)]
        public async Task IsUserValid_UserNotFoundOrUnauthorised_ShouldReturnFalse(HttpStatusCode httpStatusCode)
        {
            using var fakeAuthenticationService = _fixture.Create<HttpTest>();
            fakeAuthenticationService
                .ForCallsTo(GetUserIsValidRoute)
                .RespondWith(status: (int) httpStatusCode);

            var sut = _fixture.Create<ActisureUserStore>();

            (await sut.UserIsValid("not.me")).UserId.Should().BeNullOrWhiteSpace();
        }

        [Fact]
        public void IsUserValid_ServiceError_ShouldThrowException()
        {
            using var fakeAuthenticationService = _fixture.Create<HttpTest>();
            fakeAuthenticationService
                .ForCallsTo(GetUserIsValidRoute)
                .RespondWith(status: (int) HttpStatusCode.InternalServerError);

            var sut = _fixture.Create<ActisureUserStore>();

            Func<Task> action = async () => await sut.UserIsValid("it-is.me");

            action.Should().Throw<FlurlHttpException>();
        }

        [Fact]
        public async Task IsUserValid_ValidUser_ShouldReturnTrue()
        {
            var IsUserValidResp =
                "{\"userId\":\"me\",\"success\":true}";

            using var fakeAuthenticationService = _fixture.Create<HttpTest>();
            fakeAuthenticationService
                .ForCallsTo(GetUserIsValidRoute)
                .RespondWith(status: (int) HttpStatusCode.OK, body: IsUserValidResp);

            var sut = _fixture.Create<ActisureUserStore>();

            var resp = await sut.UserIsValid("it-is.me");
            resp.UserId.Should().Be("me");
        }

        [Fact]
        public async Task FindByUsername_ValidUser_ShouldReturnUser()
        {
            var secAccessResp =
                "{\"secAccessUserList\":[],\"secAccessTeamList\":[],\"actisureUserId\":null,\"uniqueExternalUserIdentifier\":\"it-is.me\",\"errorMessage\":null,\"success\":true,\"errors\":[]}";
            using var fakeAuthenticationService = _fixture.Create<HttpTest>();
            fakeAuthenticationService
                .ForCallsTo(GetFunctionsForUserRoute)
                .RespondWith(status: (int)HttpStatusCode.OK, body: secAccessResp);

            var sut = _fixture.Create<ActisureUserStore>();

            var resp = await sut.FindByUsername("it-is.me");
            Assert.Equal(resp.UniqueExternalUserIdentifier, "it-is.me");
        }
    }
}
