﻿using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

using AutoFixture;
using AutoFixture.AutoNSubstitute;

using FluentAssertions;

using Flurl.Http.Testing;

using IdentityModel.Client;

using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Hosting;

using Xunit;

namespace Actisure.Services.Authorization.Tests.Functional
{
    public class ResourceOwnerPasswordFlowTests : IAsyncLifetime
    {
        private readonly IFixture _fixture = new Fixture().Customize(new AutoNSubstituteCustomization());

        private IHost _host;

        public async Task InitializeAsync()
        {
            _host = await HostFactory.Create<Startup>(new[]
            {
                new KeyValuePair<string, string>("AuthenticationServiceBaseAddress", "http://fake-authentication-service")
            });

            _fixture.Register(() => _host.GetTestClient());

            _fixture.Inject(
                new PasswordTokenRequest
                {
                    Address = "/connect/token",
                    ClientId = "ActisureExternal",
                    ClientSecret = "test-secret",
                    Scope = "services",
                    UserName = "test",
                    Password = "test"
                });
        }

        public async Task DisposeAsync()
        {
            if (_host != null)
            {
                await _host.StopAsync();

                _host.Dispose();
            }
        }

        [Theory]
        [InlineData("", "Username or password is invalid.")]
        [InlineData("en-GB", "Username or password is invalid.")]
        [InlineData("fr", "Nom d'utilisateur ou mot de passe est invalide.")]
        public async Task ResourceOwnerPasswordFlow_InvalidPassword_ShouldRespondWithBadRequest(
            string acceptLanguage,
            string expectedMessage)
        {
            using var httpTest = new HttpTest();

            httpTest.SetFakeAuthenticationServiceToRespondWith(HttpStatusCode.Unauthorized);

            var client = CreateClientWithAcceptLanguageHeader(acceptLanguage);

            var response = await client.RequestPasswordTokenAsync(_fixture.Create<PasswordTokenRequest>());

            VerifyBadRequestResponse(response, expectedMessage);
        }

        private static void VerifyBadRequestResponse(TokenResponse response, string expectedMessage)
        {
            response.IsError.Should().BeTrue();
            response.ErrorType.Should().Be(ResponseErrorType.Protocol);
            response.AccessToken.Should().BeNull();
            response.ErrorDescription.Should().Be(expectedMessage);
        }

        private HttpClient CreateClientWithAcceptLanguageHeader(string acceptLanguage)
        {
            var client = _fixture.Create<HttpClient>();
            if (!string.IsNullOrEmpty(acceptLanguage))
            {
                client.DefaultRequestHeaders.AcceptLanguage.Add(new StringWithQualityHeaderValue(acceptLanguage));
            }

            return client;
        }


        [Theory]
        [InlineData("", "The server responded with a status code of 404.")]
        [InlineData("en-GB", "The server responded with a status code of 404.")]
        [InlineData("fr", "Le serveur a répondu avec un code de 404 Etat.")]
        public async Task ResourceOwnerPasswordFlow_ResourceOwnerNotFound_ShouldRespondWithBadRequest(
            string acceptLanguage,
            string expectedMessage)
        {
            using var httpTest = new HttpTest();

            httpTest.SetFakeAuthenticationServiceToRespondWith(HttpStatusCode.NotFound);

            var client = CreateClientWithAcceptLanguageHeader(acceptLanguage);

            var response = await client.RequestPasswordTokenAsync(_fixture.Create<PasswordTokenRequest>());

            VerifyBadRequestResponse(response, expectedMessage);
        }

        [Theory]
        [InlineData("", "Your password has expired.")]
        [InlineData("en-GB", "Your password has expired.")]
        [InlineData("fr", "Votre mot de passe a expiré.")]
        public async Task ResourceOwnerPasswordFlow_ExpiredPassword_ShouldRespondWithBadRequest(
            string acceptLanguage,
            string expectedMessage)
        {
            using var httpTest = new HttpTest();

            httpTest.SetFakeAuthenticationServiceToRespondWith(HttpStatusCode.Forbidden);

            var client = CreateClientWithAcceptLanguageHeader(acceptLanguage);

            var response = await client.RequestPasswordTokenAsync(_fixture.Create<PasswordTokenRequest>());

            VerifyBadRequestResponse(response, expectedMessage);
        }

        [Theory]
        [InlineData("", "The server responded with a status code of 500.")]
        [InlineData("en-GB", "The server responded with a status code of 500.")]
        [InlineData("fr", "Le serveur a répondu avec un code de 500 Etat.")]
        public async Task ResourceOwnerPasswordFlow_AuthenticationServerError_ShouldRespondWithBadRequest(
            string acceptLanguage,
            string expectedMessage)
        {
            using var httpTest = new HttpTest();

            httpTest.SetFakeAuthenticationServiceToRespondWith(HttpStatusCode.InternalServerError);

            var client = CreateClientWithAcceptLanguageHeader(acceptLanguage);

            var response = await client.RequestPasswordTokenAsync(_fixture.Create<PasswordTokenRequest>());

            VerifyBadRequestResponse(response, expectedMessage);
        }

        [Theory]
        [InlineData("")]
        [InlineData("en-GB")]
        [InlineData("fr")]
        public async Task ResourceOwnerPasswordFlow_ValidCredentials_ShouldCreateAccessToken(string acceptLanguage)
        {
            using var fakeService = new HttpTest();
            fakeService
                .RespondWithValidLogin()
                .RespondWithNoSecurityFunctions();

            var client = CreateClientWithAcceptLanguageHeader(acceptLanguage);

            var response = await client.RequestPasswordTokenAsync(_fixture.Create<PasswordTokenRequest>());

            response.AccessToken.Should().NotBeNullOrEmpty();
        }
    }
}
