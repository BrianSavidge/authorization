﻿using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

using AutoFixture;
using AutoFixture.AutoNSubstitute;

using FluentAssertions;

using IdentityModel.Client;

using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Hosting;

using Xunit;

namespace Actisure.Services.Authorization.Tests.Functional
{
    public class TokenStoreTests : IAsyncLifetime
    {
        private IHost _host;

        public async Task InitializeAsync()
        {
            _host = await HostFactory.Create<Startup>(new[]
           {
                new KeyValuePair<string, string>("ConnectionStrings:TokenStoreConnection", "Server=(local);Database=IdentityServer4.Quickstart.EntityFramework-3.0.0;Trusted_Connection=True;"),
                new KeyValuePair<string, string>("EmbeddedCertificatePassword", "TYyeB3meVHNGrpQx"),

                new KeyValuePair<string, string>("IdentityServer:Clients:0:ClientId", "client"),
                new KeyValuePair<string, string>("IdentityServer:Clients:0:ClientSecrets:0:Value", "K7gNU3sdo+OL0wNhqoVWhr3g6s1xYv72ol/pe/Unols="),
                new KeyValuePair<string, string>("IdentityServer:Clients:0:AllowedGrantTypes:0", "password"),
                new KeyValuePair<string, string>("IdentityServer:Clients:0:AllowedGrantTypes:1", "client_credentials"),
                new KeyValuePair<string, string>("IdentityServer:Clients:0:AllowedScopes:0", "openid"),
                new KeyValuePair<string, string>("IdentityServer:Clients:0:AllowedScopes:1", "services"),
                new KeyValuePair<string, string>("IdentityServer:Clients:0:AllowedScopes:2", "impersonation"),
                new KeyValuePair<string, string>("IdentityServer:Clients:0:AccessTokenType", "1"),//Reference

                new KeyValuePair<string, string>("IdentityServer:ApiScopes:0:Name", "services"),
                new KeyValuePair<string, string>("IdentityServer:ApiScopes:0:DisplayName", "My API"),
                new KeyValuePair<string, string>("IdentityServer:ApiScopes:0:UserClaims:0","act_as"),

                new KeyValuePair<string, string>("IdentityServer:ApiResources:0:Name","actisure-service"),
                new KeyValuePair<string, string>("IdentityServer:ApiResources:0:Scopes:0","services"),
                new KeyValuePair<string, string>("IdentityServer:ApiResources:0:Scopes:1","openid"),
                new KeyValuePair<string, string>("IdentityServer:ApiResources:0:ApiSecrets:0:Value","K7gNU3sdo+OL0wNhqoVWhr3g6s1xYv72ol/pe/Unols="),

                new KeyValuePair<string, string>("IdentityServer:IdentityResources:0:Name", "openid"),
                new KeyValuePair<string, string>("IdentityServer:IdentityResources:0:UserClaims:0", "act_as"),

            });

            _fixture.Register(() => _host.GetTestClient());
        }

        public async Task DisposeAsync()
        {
            if (_host != null)
            {
                await _host.StopAsync();

                _host.Dispose();
            }
        }

        private readonly IFixture _fixture = new Fixture().Customize(new AutoNSubstituteCustomization());

        [Fact(Skip="Skip until containerised database can be used")]
        public async Task CreateToken_ReferenceToken_Introspect_Should_Work()
        {
            var client = _fixture.Create<HttpClient>();

            var tokenResponse = await client.RequestClientCredentialsTokenAsync(
                new ClientCredentialsTokenRequest
                {
                    Address = "/connect/token",
                    ClientId = "client",
                    ClientSecret = "secret",
                    Scope = "services",

                });

            var accessToken = tokenResponse.AccessToken;
            accessToken.Should().NotBeNullOrEmpty();

            await InitializeAsync();

            var tokenIntrospectionResponse = await client.IntrospectTokenAsync(new TokenIntrospectionRequest
            {
                Address = "/connect/introspect",
                ClientId = "actisure-service",
                ClientSecret = "secret",

                Token = accessToken
            });

            tokenIntrospectionResponse.IsError.Should().BeFalse();
            tokenIntrospectionResponse.IsActive.Should().BeTrue();
        }
    }
}
