﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

using Actisure.Services.Authorization.Infrastructure;

using AutoFixture;
using AutoFixture.AutoNSubstitute;

using FluentAssertions;

using Flurl.Http.Testing;

using IdentityModel.Client;

using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Hosting;

using Xunit;

namespace Actisure.Services.Authorization.Tests.Functional
{
    public class ResourceOwnerPasswordFlowWithActAsTests : IAsyncLifetime
    {
        private readonly IFixture _fixture = new Fixture().Customize(new AutoNSubstituteCustomization());

        private IHost _host;

        public async Task InitializeAsync()
        {
            _host = await HostFactory.Create<Startup>(new[]
            {
                new KeyValuePair<string, string>("AuthenticationServiceBaseAddress", "http://fake-authentication-service")
            });

            _fixture.Register(() => _host.GetTestClient());

            _fixture.Register(() =>
            {
                var fakeService = new HttpTest();
                fakeService
                    .RespondWithValidLogin()
                    .RespondWithNoSecurityFunctions();

                return fakeService;
            });

            _fixture.Inject(
                new PasswordTokenRequest
                {
                    ClientId = "ActisureExternal",
                    ClientSecret = "test-secret",
                    Scope = "openid services impersonation",
                    UserName = "test",
                    Password = "test"
                });
        }

        public async Task DisposeAsync()
        {
            if (_host != null)
            {
                await _host.StopAsync();

                _host.Dispose();
            }
        }

        [Fact]
        public async Task ResourceOwnerPasswordFlow_AcrValuesWithActAsValue_ShouldAddActAsClaim()
        {
            using var fakeAuthenticationService = _fixture.Create<HttpTest>();

            var client = _fixture.Create<HttpClient>();

            var discoveryDocument = await client.GetDiscoveryDocumentAsync();

            var request = _fixture.Create<PasswordTokenRequest>();
            request.Address = discoveryDocument.TokenEndpoint;
            request.Parameters.AddOptional(Constants.AcrValues, $"{Constants.ActAs}:test.user");

            // Act
            var tokenResponse = await client.RequestPasswordTokenAsync(request);

            tokenResponse.AccessToken.Should().NotBeNullOrEmpty();

            var userInfoResponse = await client.GetUserInfoAsync(new UserInfoRequest
            {
                Address = discoveryDocument.UserInfoEndpoint,
                Token = tokenResponse.AccessToken
            });

            // Assert
            userInfoResponse.Claims.Any(c => c.Type == Constants.ActAs).Should().BeTrue();
        }

        [Fact]
        public async Task ResourceOwnerPasswordFlow_EmptyAcrValuesHeader_ShouldNotAddAdditionalClaims()
        {
            using var fakeAuthenticationService = _fixture.Create<HttpTest>();

            var client = _fixture.Create<HttpClient>();

            var discoveryDocument = await client.GetDiscoveryDocumentAsync();

            var request = _fixture.Create<PasswordTokenRequest>();
            request.Address = discoveryDocument.TokenEndpoint;

            request.Parameters.AddOptional(Constants.AcrValues, string.Empty);

            var tokenResponse = await client.RequestPasswordTokenAsync(request);

            tokenResponse.AccessToken.Should().NotBeNullOrEmpty();

            var userInfoResponse = await client.GetUserInfoAsync(new UserInfoRequest
            {
                Address = discoveryDocument.UserInfoEndpoint,
                Token = tokenResponse.AccessToken
            });

            // Assert
            userInfoResponse.Claims.Any(c => c.Type == Constants.ActAs).Should().BeFalse();
        }

        [Fact]
        public async Task ResourceOwnerPasswordFlow_MultipleAcrValuesHeader_ShouldAddActAsClaim()
        {
            using var fakeAuthenticationService = _fixture.Create<HttpTest>();

            var client = _fixture.Create<HttpClient>();

            var discoveryDocument = await client.GetDiscoveryDocumentAsync();

            var request = _fixture.Create<PasswordTokenRequest>();
            request.Address = discoveryDocument.TokenEndpoint;

            request.Parameters.AddOptional(
                Constants.AcrValues,
                $"{Constants.ActAs}:test.user other_acr_value:value another_act_value:other_value");

            var tokenResponse = await client.RequestPasswordTokenAsync(request);

            tokenResponse.AccessToken.Should().NotBeNullOrEmpty();

            var userInfoResponse = await client.GetUserInfoAsync(new UserInfoRequest
            {
                Address = discoveryDocument.UserInfoEndpoint,
                Token = tokenResponse.AccessToken
            });

            // Assert
            userInfoResponse.Claims.Any(c => c.Type == Constants.ActAs).Should().BeTrue();
        }

        [Fact]
        public async Task ResourceOwnerPasswordFlow_MultipleActAsValues_ShouldUseFirstActAsValue()
        {
            using var fakeAuthenticationService = _fixture.Create<HttpTest>();

            // Arrange
            var client = _fixture.Create<HttpClient>();
            var discoveryDocument = await client.GetDiscoveryDocumentAsync();

            var request = _fixture.Create<PasswordTokenRequest>();
            request.Address = discoveryDocument.TokenEndpoint;

            request.Parameters.AddOptional(
                Constants.AcrValues,
                $"{Constants.ActAs}:baby.bear ${Constants.ActAs}:mummy.bear ${Constants.ActAs}:daddy.bear");

            var tokenResponse = await client.RequestPasswordTokenAsync(_fixture.Create<PasswordTokenRequest>());

            tokenResponse.AccessToken.Should().NotBeNullOrEmpty();

            var userInfoResponse = await client.GetUserInfoAsync(new UserInfoRequest
            {
                Address = discoveryDocument.UserInfoEndpoint,
                Token = tokenResponse.AccessToken
            });

            // Assert
            userInfoResponse
                .Claims
                .Any(c => c.Type == Constants.ActAs && c.Value == "baby.bear")
                .Should().BeTrue($"baby.bear was the first {Constants.ActAs} value");
        }

        [Fact]
        public async Task ResourceOwnerPasswordFlow_NoAcrValues_ShouldNotAddAdditionalClaims()
        {
            using var fakeAuthenticationService = _fixture.Create<HttpTest>();

            var client = _fixture.Create<HttpClient>();

            var discoveryDocument = await client.GetDiscoveryDocumentAsync();

            var request = _fixture.Create<PasswordTokenRequest>();
            request.Address = discoveryDocument.TokenEndpoint;
            request.Parameters.Any(p => p.Key == Constants.AcrValues).Should().BeFalse();

            var tokenResponse = await client.RequestPasswordTokenAsync(request);


            tokenResponse.AccessToken.Should().NotBeNullOrEmpty();

            var userInfoResponse = await client.GetUserInfoAsync(new UserInfoRequest
            {
                Address = discoveryDocument.UserInfoEndpoint,
                Token = tokenResponse.AccessToken
            });

            // Assert
            userInfoResponse.Claims.Any(c => c.Type == Constants.ActAs).Should().BeFalse();
        }

        [Fact]
        public async Task ResourceOwnerPasswordFlue_MalformedAcrValuesHeader_ShouldReturnBadRequest()
        {
            using var httpTest = new HttpTest();
            httpTest.SetFakeAuthenticationServiceToRespondWith(HttpStatusCode.OK);

            var client = _fixture.Create<HttpClient>();

            var discoveryDocument = await client.GetDiscoveryDocumentAsync();

            var request = _fixture.Create<PasswordTokenRequest>();
            request.Address = discoveryDocument.TokenEndpoint;
            request.Parameters.AddOptional(Constants.AcrValues, $"{Constants.ActAs}test.user");

            var response = await client.RequestPasswordTokenAsync(request);

            response.ErrorType.Should().Be(ResponseErrorType.Protocol);
            response.AccessToken.Should().BeNull();
            response.ErrorDescription.Should().Be("An invalid act_as value has been supplied in the acr_values property.");
        }
    }
}
