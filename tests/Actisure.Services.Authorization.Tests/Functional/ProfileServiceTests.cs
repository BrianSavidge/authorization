﻿using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;

using Actisure.Services.Authorization.Infrastructure;

using AutoFixture;
using AutoFixture.AutoNSubstitute;

using FluentAssertions;

using Flurl.Http.Testing;

using IdentityModel;
using IdentityModel.Client;

using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Hosting;

using Xunit;

namespace Actisure.Services.Authorization.Tests.Functional
{
    public class ProfileServiceTests : IAsyncLifetime
    {
        private readonly IFixture _fixture = new Fixture().Customize(new AutoNSubstituteCustomization());

        private IHost _host;

        public async Task InitializeAsync()
        {
            _host = await HostFactory.Create<Startup>(new[]
            {
                new KeyValuePair<string, string>("AuthenticationServiceBaseAddress", "http://fake-authentication-service")
            });

            _fixture.Register(() => _host.GetTestClient());

            _fixture.Register(() =>
            {
                var fakeService = new HttpTest();
                fakeService.RespondWithValidLogin();

                return fakeService;
            });

            _fixture.Inject(
                new PasswordTokenRequest
                {
                    ClientId = "ActisureExternal",
                    ClientSecret = "test-secret",
                    Scope = "openid services names actisure",
                    UserName = "test",
                    Password = "test"
                });
        }

        public async Task DisposeAsync()
        {
            if (_host != null)
            {
                await _host.StopAsync();

                _host.Dispose();
            }
        }

        [Fact]
        public async Task ResourceOwnerPasswordFlow_ValidClaimsUser_AddsClaimsClaim()
        {
            using var fakeAuthenticationService = _fixture.Create<HttpTest>();
            fakeAuthenticationService
                .RespondWithJson(new
                {
                    Success = true,
                    SecAccessUserList = new[]
                    {
                        new
                        {
                            SecCategory = "CLAIMS",
                            SecFunction = "CLAIMS",
                            SecView = true
                        }
                    }
                });

            var client = _fixture.Create<HttpClient>();

            var discoveryDocument = await client.GetDiscoveryDocumentAsync();

            var tokenRequest = _fixture.Create<PasswordTokenRequest>();
            tokenRequest.Address = discoveryDocument.TokenEndpoint;

            // Act
            var tokenResponse = await client.RequestPasswordTokenAsync(tokenRequest);

            tokenResponse.AccessToken.Should().NotBeNullOrEmpty();

            var userInfoResponse = await client.GetUserInfoAsync(new UserInfoRequest
            {
                Address = discoveryDocument.UserInfoEndpoint,
                Token = tokenResponse.AccessToken
            });

            // Assert
            userInfoResponse.Claims.Any(c => c.Type == Constants.IsClaimsUser && c.Value == true.ToString()).Should().BeTrue();
        }

        [Fact]
        public async Task ResourceOwnerPasswordFlow_ValidPolicyUser_AddsPolicyClaim()
        {
            using var fakeAuthenticationService = _fixture.Create<HttpTest>();
            fakeAuthenticationService
                .ForCallsTo("*getfunctionsforuser*")
                .RespondWithJson(new
                {
                    Success = true,
                    SecAccessUserList = new[]
                    {
                        new
                        {
                            SecCategory = "POLICY",
                            SecFunction = "POLICY",
                            SecView = true
                        }
                    }
                });

            var client = _fixture.Create<HttpClient>();

            var discoveryDocument = await client.GetDiscoveryDocumentAsync();

            var tokenRequest = _fixture.Create<PasswordTokenRequest>();
            tokenRequest.Address = discoveryDocument.TokenEndpoint;

            // Act
            var tokenResponse = await client.RequestPasswordTokenAsync(tokenRequest);

            tokenResponse.AccessToken.Should().NotBeNullOrEmpty();

            var userInfoResponse = await client.GetUserInfoAsync(new UserInfoRequest
            {
                Address = discoveryDocument.UserInfoEndpoint,
                Token = tokenResponse.AccessToken
            });

            // Assert
            userInfoResponse.Claims.Any(c => c.Type == Constants.IsPolicyUser && c.Value == true.ToString()).Should().BeTrue();
        }

        [Fact]
        public async Task ResourceOwnerPasswordFlow_ValidSupervisor_AddsSupervisorClaim()
        {
            using var fakeAuthenticationService = _fixture.Create<HttpTest>();
            fakeAuthenticationService
                .ForCallsTo("*getfunctionsforuser*")
                .RespondWithJson(new
                {
                    Success = true,
                    SecAccessUserList = new[]
                    {
                        new
                        {
                            SecFunction = "SUPERVISOR",
                            SecView = true
                        }
                    }
                });

            var client = _fixture.Create<HttpClient>();

            var discoveryDocument = await client.GetDiscoveryDocumentAsync();

            var tokenRequest = _fixture.Create<PasswordTokenRequest>();
            tokenRequest.Address = discoveryDocument.TokenEndpoint;

            // Act
            var tokenResponse = await client.RequestPasswordTokenAsync(tokenRequest);

            tokenResponse.AccessToken.Should().NotBeNullOrEmpty();

            var userInfoResponse = await client.GetUserInfoAsync(new UserInfoRequest
            {
                Address = discoveryDocument.UserInfoEndpoint,
                Token = tokenResponse.AccessToken
            });

            // Assert
            userInfoResponse.Claims.Any(c => c.Type == Constants.IsSupervisor && c.Value == true.ToString()).Should().BeTrue();
        }

        [Fact]
        public async Task ResourceOwnerPasswordFlow_ValidUserButNoSecurityFunctions_AddsBasicClaims()
        {
            using var fakeAuthenticationService = _fixture.Create<HttpTest>();
            fakeAuthenticationService.RespondWithNoSecurityFunctions();

            var client = _fixture.Create<HttpClient>();

            var discoveryDocument = await client.GetDiscoveryDocumentAsync();

            var tokenRequest = _fixture.Create<PasswordTokenRequest>();
            tokenRequest.Address = discoveryDocument.TokenEndpoint;

            // Act
            var tokenResponse = await client.RequestPasswordTokenAsync(tokenRequest);

            tokenResponse.AccessToken.Should().NotBeNullOrEmpty();

            var userInfoResponse = await client.GetUserInfoAsync(new UserInfoRequest
            {
                Address = discoveryDocument.UserInfoEndpoint,
                Token = tokenResponse.AccessToken
            });

            // Assert
            userInfoResponse.Claims.Any(c => c.Type == JwtClaimTypes.Subject && c.Value == tokenRequest.UserName).Should().BeTrue();
            userInfoResponse.Claims.Any(c => c.Type == JwtClaimTypes.GivenName && c.Value == tokenRequest.UserName).Should().BeTrue();
            userInfoResponse.Claims.Any(c => c.Type == JwtClaimTypes.Name && c.Value == tokenRequest.UserName).Should().BeTrue();
            userInfoResponse.Claims.Any(c => c.Type == ClaimTypes.NameIdentifier && c.Value == tokenRequest.UserName).Should().BeTrue();
        }

        [Fact]
        public async Task ResourceOwnerPasswordFlow_ValidUserWithAuthorityLimit_AddsLimitClaim()
        {
            using var fakeAuthenticationService = _fixture.Create<HttpTest>();
            fakeAuthenticationService
                .ForCallsTo("*getfunctionsforuser*")
                .RespondWithJson(new
                {
                    Success = true,
                    SecAccessUserList = new[]
                    {
                        new
                        {
                            SecCategory = "CLAIMS",
                            SecFunction = "AUTHORITYLIMIT",
                            SecOther2 = "606"
                        }
                    }
                });

            var client = _fixture.Create<HttpClient>();

            var discoveryDocument = await client.GetDiscoveryDocumentAsync();

            var tokenRequest = _fixture.Create<PasswordTokenRequest>();
            tokenRequest.Address = discoveryDocument.TokenEndpoint;

            // Act
            var tokenResponse = await client.RequestPasswordTokenAsync(tokenRequest);

            tokenResponse.AccessToken.Should().NotBeNullOrEmpty();

            var userInfoResponse = await client.GetUserInfoAsync(new UserInfoRequest
            {
                Address = discoveryDocument.UserInfoEndpoint,
                Token = tokenResponse.AccessToken
            });

            // Assert
            userInfoResponse.Claims.Any(c => c.Type == Constants.HasAuthorityLimit && c.Value == true.ToString()).Should().BeTrue();
            userInfoResponse.Claims.Any(c => c.Type == Constants.AuthorityLimitAmount && c.Value == "606").Should().BeTrue();
        }
    }
}
