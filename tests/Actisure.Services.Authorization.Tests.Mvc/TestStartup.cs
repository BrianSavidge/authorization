﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Actisure.Services.Authorization.Tests.Mvc
{
    internal class TestStartup : Startup
    {
        public void ConfigureTestServices(IServiceCollection services)
        {
            ConfigureServices(services);

            // Replace only your own custom services. The ASP.NET Core ones 
            // are already replaced by MyTested.AspNetCore.Mvc. 
            //services.Replace<IService, MockedService>();
        }

        public TestStartup(IConfiguration configuration, IWebHostEnvironment environment) : base(configuration, environment)
        {
        }
    }
}
