﻿using Actisure.Services.Authorization.Presentation.Home;

using IdentityServer4.Services;

using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

using MyTested.AspNetCore.Mvc;
using MyTested.AspNetCore.Mvc.Internal;

using NSubstitute;

using Xunit;

namespace Actisure.Services.Authorization.Tests.Mvc
{
    public class HomeControllerShould
    {
        [Fact]
        public void ReturnNotOkWithCorrectModelWhenCallingAuthenticatedIndexAction()
        {
            IWebHostEnvironment env =
                new TestHostEnvironment
                {
                    EnvironmentName = Environments.Production
                };

            MyMvc // Start a test case.
                .Controller<HomeController>(instance =>
                    instance
                        .WithDependencies(dependencies =>
                            dependencies
                                .WithNo<IIdentityServerInteractionService>()
                                .With(env)
                                .With(Substitute.For<ILogger<HomeController>>())))
                .Calling(c => c.Index()) // Act - invoke the action under test.
                .ShouldReturn() // Assert action behavior.
                .NotFound(); // Assert specific model properties. 
        }

        [Fact]
        public void ReturnOkWithCorrectModelWhenCallingAuthenticatedIndexAction()
        {
            IWebHostEnvironment env =
                new TestHostEnvironment
                {
                    EnvironmentName = Environments.Development
                };

            MyMvc // Start a test case.
                .Controller<HomeController>(instance =>
                    instance
                        .WithDependencies(dependencies =>
                            dependencies
                                .WithNo<IIdentityServerInteractionService>()
                                .With(env)
                                .WithNo<ILogger<HomeController>>()))
                .Calling(c => c.Index()) // Act - invoke the action under test.
                .ShouldReturn() // Assert action behavior.
                .View(); // Assert specific model properties. 
        }
    }
}
