﻿using Actisure.Services.Authorization.Presentation.Account;

using MyTested.AspNetCore.Mvc;

using Xunit;

namespace Actisure.Services.Authorization.Tests.Mvc
{
    public class AccountControllerShould
    {
        [Fact]
        public void ReturnOkWithCorrectModelWhenCallingAuthenticatedIndexAction()
        {
            MyMvc // Start a test case.
                .Controller<AccountController>()
                .Calling(c => c.Login("return-url")) // Act - invoke the action under test.
                .ShouldReturn() // Assert action behavior.
                .View(result => result.WithModelOfType<LoginViewModel>().Passing(model => model.ReturnUrl == "return-url")); // Assert specific model properties. 
        }
    }
}
