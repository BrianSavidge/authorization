﻿// Copyright (c) Brock Allen & Dominick Baier. All rights reserved.
// Licensed under the Apache License, Version 2.0. See LICENSE in the project root for license information.


using System;

namespace Actisure.Services.Authorization.Presentation.Account
{
    public static class AccountOptions
    {
        public const bool AllowLocalLogin = true;

        public const bool ShowLogoutPrompt = true;

        public const bool AutomaticRedirectAfterSignOut = false;

        public const string InvalidCredentialsErrorMessage = "Invalid username or password";

        public static readonly TimeSpan s_rememberMeLoginDuration = TimeSpan.FromDays(30);
    }
}
