﻿using System.Reflection;

using Actisure.Services.Authorization.Infrastructure.Certificates;
using Actisure.Services.Authorization.Infrastructure.ProfileService;

using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Actisure.Services.Authorization.Infrastructure
{
    public static class ServiceCollectionExtensions
    {
        public static void ConfigureViewLocations(this IServiceCollection services)
        {
            services.Configure<RazorViewEngineOptions>(o =>
            {
                // {2} is area, {1} is controller,{0} is the action    
                o.ViewLocationFormats.Clear();
                o.ViewLocationFormats.Add("/Presentation/{1}/Views/{0}" + RazorViewEngine.ViewExtension);
                o.ViewLocationFormats.Add("/Presentation/{1}/{0}" + RazorViewEngine.ViewExtension);
                o.ViewLocationFormats.Add("/Presentation/Shared/{0}" + RazorViewEngine.ViewExtension);
                o.ViewLocationFormats.Add("/Presentation/Shared/Views/{0}" + RazorViewEngine.ViewExtension);
            });
        }

        public static void ConfigureIdentityServer(
            this IServiceCollection services,
            IConfiguration configuration,
            IWebHostEnvironment environment)
        {
            var secureHost = configuration.GetValue(ConfigurationKeys.SecureHost, true);

            var builder =
                services
                    .AddIdentityServer(options =>
                    {
                        if (!secureHost)
                        {
                            options.Authentication.CookieSameSiteMode = SameSiteMode.Lax;
                            options.Authentication.CheckSessionCookieSameSiteMode = SameSiteMode.Lax;
                        }
                    })
                    .AddInMemoryApiScopes(configuration.GetSection("IdentityServer:ApiScopes"))
                    .AddInMemoryClients(configuration.GetSection("IdentityServer:Clients"))
                    .AddResourceOwnerValidator<ActisureResourceOwnerPasswordValidator>()
                    .AddProfileService<ActisureProfileService>()
                    .AddInMemoryApiResources(configuration.GetSection("IdentityServer:ApiResources"))
                    .AddInMemoryIdentityResources(configuration.GetSection("IdentityServer:IdentityResources"));

            var connectionString = configuration.GetConnectionString(ConfigurationKeys.TokenStoreConnectionString);

            if (!string.IsNullOrEmpty(connectionString))
            {
                var migrationsAssembly = typeof(Startup).GetTypeInfo().Assembly.GetName().Name;

                builder
                    .AddOperationalStore(options =>
                    {
                        options.ConfigureDbContext = b => b.UseSqlServer(connectionString, sql => sql.MigrationsAssembly(migrationsAssembly));

                        options.EnableTokenCleanup = configuration.GetValue(ConfigurationKeys.EnableTokenCleanup, false);

                        var tokenCleanupInterval = configuration.GetValue(ConfigurationKeys.TokenCleanupInterval, 3600);
                        if (tokenCleanupInterval > 0)
                        {
                            options.TokenCleanupInterval = tokenCleanupInterval;
                        }
                    });
            }

            if (environment.IsDevelopment())
            {
                builder.AddDeveloperSigningCredential();
            }
            else
            {
                var options = new CertificateOptions();
                configuration.GetSection(ConfigurationKeys.Certificate).Bind(options);

                builder.AddSigningCredential(CertificateProvider.GetCertificate(options));
            }
        }
    }
}
