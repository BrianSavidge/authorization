﻿using System.Collections.Generic;
using System.Security.Claims;

using IdentityModel;

namespace Actisure.Services.Authorization.Infrastructure
{
    public class User
    {
        /// <summary>
        /// Gets or sets the claims.
        /// </summary>
        public ICollection<Claim> Claims { get; set; } = new HashSet<Claim>(new ClaimComparer());

        /// <summary>
        /// Gets or sets if the user is active.
        /// </summary>
        public bool IsActive { get; set; } = true;

        /// <summary>
        /// Gets or sets the subject identifier.
        /// </summary>
        public string SubjectId { get; set; }

        /// <summary>
        /// Gets or sets the username.
        /// </summary>
        public string ActisureUserId { get; set; }

        public string UniqueExternalUserIdentifier { get; set; }
    }
}
