﻿namespace Actisure.Services.Authorization.Infrastructure
{
    public static class Constants
    {
        public const string AcrValues = "acr_values";

        public const string ActAs = "act_as";

        public const string IsPolicyUser = "IsPolicyUser";

        public const string IsClaimsUser = "IsClaimsUser";

        public const string IsSupervisor = "IsSupervisor";

        public const string HasAuthorityLimit = "HasAuthorityLimit";

        public const string AuthorityLimitAmount = "AuthorityLimitAmount";
    }
}
