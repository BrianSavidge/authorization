﻿using System;

using IdentityServer4.EntityFramework.DbContexts;

using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace Actisure.Services.Authorization.Infrastructure
{
    public static class ApplicationBuilderExtensions
    {
        public static void UseLocalization(this IApplicationBuilder app)
        {
            var supportedCultures = new[]
            {
                "en-GB",
                "fr"
            };

            var localizationOptions =
                new RequestLocalizationOptions()
                    .SetDefaultCulture(supportedCultures[0])
                    .AddSupportedCultures(supportedCultures)
                    .AddSupportedUICultures(supportedCultures);

            app.UseRequestLocalization(localizationOptions);
        }

        public static void UsePersistedTokenStore(this IApplicationBuilder app)
        {
            var serviceScopeFactory = app.ApplicationServices.GetService<IServiceScopeFactory>();

            if (serviceScopeFactory == null)
            {
                throw new InvalidOperationException("Unable to resolve ServiceScopeFactory");
            }

            using var serviceScope = serviceScopeFactory.CreateScope();
            serviceScope.ServiceProvider.GetRequiredService<PersistedGrantDbContext>().Database.Migrate();
        }
    }
}
