﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

using Actisure.Services.Authorization.Infrastructure.Resources;

using IdentityModel;

using IdentityServer4.Models;
using IdentityServer4.Validation;

using Microsoft.Extensions.Localization;

namespace Actisure.Services.Authorization.Infrastructure
{
    public class ActisureResourceOwnerPasswordValidator : IResourceOwnerPasswordValidator
    {
        private readonly IStringLocalizer<Shared> _localizer;

        private readonly ActisureUserStore _userStore;

        public ActisureResourceOwnerPasswordValidator(
            ActisureUserStore userStore,
            IStringLocalizer<Shared> localizer)
        {
            _userStore = userStore ?? throw new ArgumentNullException(nameof(userStore));
            _localizer = localizer ?? throw new ArgumentNullException(nameof(localizer));
        }

        public async Task ValidateAsync(ResourceOwnerPasswordValidationContext context)
        {
            var additionalClaims = new List<Claim>
            {
                new Claim(JwtClaimTypes.Name, context.UserName)
            };

            // Are there any ACR values in the request?
            var acrValues = context.Request.Raw[Constants.AcrValues];

            // Is there a request to 'act as' someone else?
            if (!string.IsNullOrWhiteSpace(acrValues) && acrValues.Contains(Constants.ActAs))
            {
                if (!AcrValuesParser.TryParse(acrValues, Constants.ActAs, out var actAs))
                {
                    context.Result =
                        new GrantValidationResult(
                            TokenRequestErrors.InvalidGrant,
                            _localizer["An invalid act_as value has been supplied in the acr_values property."]);

                    return;
                }

                additionalClaims.Add(new Claim(Constants.ActAs, actAs));
            }

            context.Result = await _userStore.ValidateCredentials(context.UserName, context.Password, additionalClaims);
        }
    }
}
