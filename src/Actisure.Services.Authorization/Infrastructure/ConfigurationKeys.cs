﻿namespace Actisure.Services.Authorization.Infrastructure
{
    public static class ConfigurationKeys
    {
        public const string AuthenticationServiceBaseAddress = "AuthenticationServiceBaseAddress";
        
        public const string Certificate = "Certificate";

        public const string EnableTokenCleanup = "EnableTokenCleanup";

        public const string SecureHost = "SecureHost";

        public const string TokenCleanupInterval = "TokenCleanupInterval";

        public const string TokenStoreConnectionString = "TokenStoreConnection";
    }
}
