﻿namespace Actisure.Services.Authorization.Infrastructure.ProfileService
{
    public class SecurityFunctionsModel
    {
        public string AuthorityLimitAmount { get; set; }

        public bool HasAuthorityLimit { get; set; }

        public bool IsClaimsUser { get; set; }

        public bool IsPolicyUser { get; set; }

        public bool IsSupervisor { get; set; }
    }
}
