﻿using System.Collections.Generic;

namespace Actisure.Services.Authorization.Infrastructure.ProfileService
{
    public class SecAccessResponse
    {
        public IList<SecAccess> SecAccessTeamList { get; set; } = new List<SecAccess>();

        public IList<SecAccess> SecAccessUserList { get; set; } = new List<SecAccess>();

        public bool Success { get; set; }
        public string ActisureUserId { get; set; }
        public string UniqueExternalUserIdentifier { get; set; }
    }
}
