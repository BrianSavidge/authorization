﻿namespace Actisure.Services.Authorization.Infrastructure.ProfileService
{
    public class SecAccess
    {
        public string SecCategory { get; set; }

        public string SecFunction { get; set; }

        public string SecOther2 { get; set; }

        public bool SecView { get; set; }
    }
}
