﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

using IdentityModel;

using IdentityServer4.Models;
using IdentityServer4.Services;

using Microsoft.Extensions.Logging;

namespace Actisure.Services.Authorization.Infrastructure.ProfileService
{
    public class ActisureProfileService : IProfileService
    {
        private readonly ILogger<ActisureProfileService> _logger;

        private readonly ActisureUserStore _userStore;


        public ActisureProfileService(ActisureUserStore userStore, ILogger<ActisureProfileService> logger)
        {
            _userStore = userStore ?? throw new ArgumentNullException(nameof(userStore));
            _logger = logger;
        }

        public async Task GetProfileDataAsync(ProfileDataRequestContext context)
        {
            context.LogProfileRequest(_logger);

            var username = context.Subject.FindFirst(item => item.Type == JwtClaimTypes.Subject)?.Value;

            var requestedClaims = new List<Claim>
            {
                new Claim(JwtClaimTypes.GivenName, username),
                new Claim(JwtClaimTypes.Name, username),
                new Claim(ClaimTypes.NameIdentifier, username),
                new Claim(ClaimTypes.Name, username)
            };

            var actAsClaim = context.Subject.FindFirst(Constants.ActAs);
            if (actAsClaim != null)
            {
                requestedClaims.Add(new Claim(actAsClaim.Type, actAsClaim.Value));
            }

            context.AddRequestedClaims(requestedClaims);

            var user = await _userStore.FindByUsername(username);

            context.AddRequestedClaims(user.Claims);

            context.LogIssuedClaims(_logger);
        }

        public Task IsActiveAsync(IsActiveContext context)
        {
            return Task.CompletedTask;
        }
    }
}
