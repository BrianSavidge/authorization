﻿using System.Linq;

namespace Actisure.Services.Authorization.Infrastructure.ProfileService
{
    public static class SecurityFunctionsModelFactory
    {
        public static SecurityFunctionsModel Create(SecAccessResponse response)
        {
            var securityFunctionsModel = new SecurityFunctionsModel
            {
                IsClaimsUser = AssembleClaimsUser(response),
                IsPolicyUser = AssemblePolicyUser(response),
                IsSupervisor = AssembleSupervisorUser(response),
                HasAuthorityLimit = AssembleAuthorityLimitUser(response)
            };

            if (securityFunctionsModel.HasAuthorityLimit)
            {
                AssembleAuthorityLimit(response, securityFunctionsModel);
            }
            else
            {
                securityFunctionsModel.AuthorityLimitAmount = "0";
            }

            return securityFunctionsModel;
        }

        private static bool AssembleClaimsUser(SecAccessResponse functions)
        {
            return functions.SecAccessUserList.Any(x => x.SecCategory == "CLAIMS" && x.SecFunction == "CLAIMS" && x.SecView) || functions.SecAccessTeamList.Any(x => x.SecCategory == "CLAIMS" && x.SecFunction == "CLAIMS" && x.SecView);
        }

        private static bool AssemblePolicyUser(SecAccessResponse functions)
        {
            return functions.SecAccessUserList.Any(x => x.SecCategory == "POLICY" && x.SecFunction == "POLICY" && x.SecView) || functions.SecAccessTeamList.Any(x => x.SecCategory == "POLICY" && x.SecFunction == "POLICY" && x.SecView);
        }

        private static bool AssembleSupervisorUser(SecAccessResponse functions)
        {
            return functions.SecAccessUserList.Any(x => x.SecFunction == "SUPERVISOR" && x.SecView) || functions.SecAccessTeamList.Any(x => x.SecFunction == "SUPERVISOR" && x.SecView);
        }

        private static void AssembleAuthorityLimit(
            SecAccessResponse functions,
            SecurityFunctionsModel securityFunctionsModel)
        {
            var authorityLimitForUser =
                functions.SecAccessUserList.SingleOrDefault(x => x.SecCategory == "CLAIMS" && x.SecFunction == "AUTHORITYLIMIT");

            var authorityLimitForTeam =
                functions.SecAccessTeamList.Where(x => x.SecCategory == "CLAIMS" && x.SecFunction == "AUTHORITYLIMIT")
                    .Select(x => x.SecOther2).Max();

            if (authorityLimitForUser != null && authorityLimitForUser.SecOther2 != null)
            {
                securityFunctionsModel.AuthorityLimitAmount = authorityLimitForUser.SecOther2;
            }
            else
            {
                securityFunctionsModel.AuthorityLimitAmount = authorityLimitForTeam ?? "0";
            }
        }

        private static bool AssembleAuthorityLimitUser(SecAccessResponse functions)
        {
            return functions.SecAccessUserList.Any(x => x.SecCategory == "CLAIMS" && x.SecFunction == "AUTHORITYLIMIT") || functions.SecAccessTeamList.Any(x => x.SecCategory == "CLAIMS" && x.SecFunction == "AUTHORITYLIMIT");
        }
    }
}
