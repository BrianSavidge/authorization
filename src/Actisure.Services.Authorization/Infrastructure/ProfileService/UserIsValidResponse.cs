﻿namespace Actisure.Services.Authorization.Infrastructure.ProfileService
{
    public class UserIsValidResponse
    {
        public string UserId { get; set; }
    }
}
