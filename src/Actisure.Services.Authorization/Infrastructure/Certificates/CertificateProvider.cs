﻿using System;
using System.Security.Cryptography.X509Certificates;

namespace Actisure.Services.Authorization.Infrastructure.Certificates
{
    public static class CertificateProvider
    {
        public static X509Certificate2 GetCertificate(CertificateOptions options)
        {
            if (options.Source.Equals(CertificateSources.Embedded, StringComparison.OrdinalIgnoreCase))
            {
                return new X509Certificate2(
                    "Actisure.Services.Authorization.pfx", 
                    options.EmbeddedCertificatePassword, 
                    X509KeyStorageFlags.EphemeralKeySet);
            }

            if (string.IsNullOrWhiteSpace(options.StoreName))
            {
                throw new ArgumentException("Certificate StoreName is invalid.");
            }

            if (string.IsNullOrWhiteSpace(options.Thumbprint))
            {
                throw new ArgumentException("Certificate Thumbprint is invalid.");
            }

            var certificateStore = new X509Store(options.StoreName, StoreLocation.LocalMachine);

            certificateStore.Open(OpenFlags.ReadOnly | OpenFlags.OpenExistingOnly);

            var certificates =
                certificateStore.Certificates.Find(
                    X509FindType.FindByThumbprint,
                    options.Thumbprint,
                    true);

            if (certificates.Count == 0)
            {
                throw new InvalidOperationException(
                    $"Valid certificate not found. StoreName='{options.StoreName}' Thumbprint='{options.Thumbprint}'.");
            }

            var certificate = certificates[0];

            if (!certificate.HasPrivateKey)
            {
                throw new InvalidOperationException(
                    $"Certificate exists but it does not have a private key. StoreName='{options.StoreName}' Thumbprint='{options.Thumbprint}'.");
            }

            return certificate;
        }
    }
}
