﻿namespace Actisure.Services.Authorization.Infrastructure.Certificates
{
    public class CertificateOptions
    {
        public string EmbeddedCertificatePassword { get; set; }
        public string Source { get; set; } = CertificateSources.Embedded;
        public string StoreName { get; set; }
        public string Thumbprint { get; set; }
    }
}
