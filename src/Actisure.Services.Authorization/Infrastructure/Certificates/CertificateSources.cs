﻿namespace Actisure.Services.Authorization.Infrastructure.Certificates
{
    public static class CertificateSources
    {
        public const string Embedded = "Embedded";

        public const string CertificateStore = "CertificateStore";
    }
}
