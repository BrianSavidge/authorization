﻿using System;
using System.Linq;

namespace Actisure.Services.Authorization.Infrastructure
{
    public static class AcrValuesParser
    {
        public static bool TryParse(string acrValues, string key, out string result)
        {
            result = null;

            if (string.IsNullOrWhiteSpace(acrValues) || string.IsNullOrWhiteSpace(key))
            {
                return false;
            }

            var valuePairs = acrValues.Split(' ', StringSplitOptions.RemoveEmptyEntries);

            if (!valuePairs.Any(p => p.Contains(key)))
            {
                return false;
            }

            var firstSoughtValue = valuePairs.First(p => p.Contains(key));

            var valueParts = firstSoughtValue.Split(':', StringSplitOptions.RemoveEmptyEntries);

            if (valueParts.Length < 2)
            {
                return false;
            }

            result = valueParts[1];

            return true;
        }
    }
}
