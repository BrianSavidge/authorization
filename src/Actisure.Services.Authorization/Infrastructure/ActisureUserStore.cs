﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mime;
using System.Security.Claims;
using System.Threading.Tasks;
using Actisure.Services.Authorization.Infrastructure.ProfileService;
using Actisure.Services.Authorization.Infrastructure.Resources;
using WebUtility = Actisure.Frameworks.Services.Infrastructure.NetFx.Net.WebUtility;

using Flurl.Http;

using IdentityServer4.Models;
using IdentityServer4.Validation;

using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Localization;

namespace Actisure.Services.Authorization.Infrastructure
{
    public class ActisureUserStore
    {
        private const string AuthenticationServicePathPrefix = "api/authentication/";

        private readonly string _authenticationServiceBaseAddress;

        private readonly IStringLocalizer<Shared> _localizer;

        public ActisureUserStore(
            IConfiguration configuration,
            IStringLocalizer<Shared> localizer)
        {
            _authenticationServiceBaseAddress = configuration[ConfigurationKeys.AuthenticationServiceBaseAddress] ?? throw new ArgumentNullException(nameof(configuration));
            _localizer = localizer ?? throw new ArgumentNullException(nameof(localizer));
        }

        private IFlurlRequest CreateBaseRequest()
        {
            return _authenticationServiceBaseAddress.WithHeader("accept", MediaTypeNames.Application.Json);
        }

        private IFlurlRequest CreateLoginRequest()
        {
            return CreateBaseRequest().AppendPathSegments(AuthenticationServicePathPrefix, "login/");
        }

        private IFlurlRequest CreateGetUserFunctionsRequest(string username)
        {
            return CreateBaseRequest().AppendPathSegments(AuthenticationServicePathPrefix, "getfunctionsforuser/", $"{username}/");
        }

        private IFlurlRequest CreateGetUserIsValidRequest(string username)
        {
            return CreateBaseRequest().AppendPathSegments(AuthenticationServicePathPrefix, "getuserisvalid/", $"{username}/");
        }



        public async Task<GrantValidationResult> ValidateCredentials(string username, string password, IEnumerable<Claim> claims = null)
        {
            var result = new GrantValidationResult(username, "custom", claims ?? Enumerable.Empty<Claim>());

            try
            {
                await CreateLoginRequest()
                    .PostJsonAsync(
                        new
                        {
                            UserId = username,
                            password
                        });
            }
            catch (FlurlHttpException httpException)
            {
                result = new GrantValidationResult(TokenRequestErrors.InvalidGrant)
                {
                    ErrorDescription = httpException.StatusCode switch
                    {
                        (int) HttpStatusCode.Unauthorized => _localizer["Username or password is invalid."],
                        (int) HttpStatusCode.Forbidden => _localizer["Your password has expired."],
                        _ => _localizer["The server responded with a status code of {0}.", httpException.StatusCode]
                    }
                };
            }

            return result;
        }

        public async Task<User> FindByUsername(string username)
        {

            var encodedUserName = WebUtility.Base64UrlEncode(username);

            var response = await CreateGetUserFunctionsRequest(encodedUserName).GetJsonAsync<SecAccessResponse>();

            var user = new User
            {
                SubjectId = response.ActisureUserId,
                ActisureUserId = response.ActisureUserId,
                UniqueExternalUserIdentifier = response.UniqueExternalUserIdentifier
            };

            var model = SecurityFunctionsModelFactory.Create(response);

            if (model.IsPolicyUser)
            {
                user.Claims.Add(new Claim(Constants.IsPolicyUser, true.ToString()));
            }

            if (model.IsClaimsUser)
            {
                user.Claims.Add(new Claim(Constants.IsClaimsUser, true.ToString()));
            }

            if (model.IsSupervisor)
            {
                user.Claims.Add(new Claim(Constants.IsSupervisor, true.ToString()));
            }

            if (model.HasAuthorityLimit)
            {
                user.Claims.Add(new Claim(Constants.HasAuthorityLimit, true.ToString()));
                user.Claims.Add(new Claim(Constants.AuthorityLimitAmount, model.AuthorityLimitAmount));
            }

            return user;
        }

        public async Task<UserIsValidResponse> UserIsValid(string username)
        {
            var encodedUserName = WebUtility.Base64UrlEncode(username);
            var userIsValidResponse = new UserIsValidResponse();

            try
            {
                userIsValidResponse = await CreateGetUserIsValidRequest(encodedUserName).GetJsonAsync<UserIsValidResponse>();
                
            }
            catch (FlurlHttpException httpException)
            {
                switch (httpException.StatusCode)
                {
                    case (int) HttpStatusCode.NotFound:
                    case (int) HttpStatusCode.Unauthorized:
                        return userIsValidResponse;
                }

                throw;
            }

            return userIsValidResponse;
        }
    }
}
