﻿using Actisure.Services.Authorization.Infrastructure;

using JetBrains.Annotations;

using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

using Serilog;

[assembly: AspMvcViewLocationFormat(@"~/Presentation/{1}/Views/{0}.cshtml")]
[assembly: AspMvcViewLocationFormat(@"~/Presentation/{1}/{0}.cshtml")]
[assembly: AspMvcViewLocationFormat(@"~/Presentation/Shared/{0}.cshtml")]
[assembly: AspMvcViewLocationFormat(@"~/Presentation/Shared/Views/{0}.cshtml")]

namespace Actisure.Services.Authorization
{
    public class Startup
    {
        public Startup(IConfiguration configuration, IWebHostEnvironment environment)
        {
            Environment = environment;
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public IWebHostEnvironment Environment { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddTransient<ActisureUserStore>();

            services.Configure<IISServerOptions>(iis =>
            {
                iis.AuthenticationDisplayName = "Windows";
                iis.AutomaticAuthentication = false;
            });

            services.AddHealthChecks();

            services.AddLocalization(opts => { opts.ResourcesPath = "Infrastructure\\Resources"; });

            services.AddControllersWithViews();

            services.ConfigureViewLocations();

            services.ConfigureIdentityServer(Configuration, Environment);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app)
        {
            if (Environment.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseSerilogRequestLogging();

            app.UseLocalization();

            var connectionString = Configuration.GetConnectionString(ConfigurationKeys.TokenStoreConnectionString);
            if (!string.IsNullOrEmpty(connectionString))
            {
                app.UsePersistedTokenStore();
            }

            app.UseStaticFiles();
            app.UseRouting();

            app.UseIdentityServer();

            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapHealthChecks("/health");
                endpoints.MapDefaultControllerRoute();
            });
        }
    }
}
