Write-Output ""
. "./tools/scripts/version.ps1"
. "./tools/scripts/settings.ps1"

if(Test-Path $outputDirectory) { rm $outputDirectory -Force -Recurse }

$publishDirectory = "$outputDirectory/publish/$name"

Write-Output "   publishDirectory:     $($publishDirectory)"
Write-Output ""

dotnet publish `
    ".\src\$($buildSettings.name)\$($buildSettings.name).$($buildSettings.projectType)" `
    -c $configuration `
    -o $publishDirectory `
    /p:Version=$version `
    /p:AssemblyVersion=$assemblyVersion `
    /p:FileVersion=$fileVersion `
    /p:InformationalVersion=$informationalVersion